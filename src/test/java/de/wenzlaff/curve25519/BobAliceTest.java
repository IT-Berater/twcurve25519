package de.wenzlaff.curve25519;

import org.junit.jupiter.api.Test;

/**
 * Test mit Alice und Bob.
 * 
 * @author Thomas Wenzlaff
 */
class BobAliceTest {

	@Test
	void testMain() {
		String[] parameter = { "" };
		BobAlice.main(parameter);
	}
}