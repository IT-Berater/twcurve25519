package de.wenzlaff.curve25519;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.whispersystems.curve25519.Curve25519;
import org.whispersystems.curve25519.Curve25519KeyPair;

/**
 * Curve 25519 Sample https://github.com/IT-Berater/curve25519-java.git
 * 
 * @author Thomas Wenzlaff
 *
 */
public class Check {

	private static final Logger LOG = LogManager.getLogger(Check.class);

	public static void main(String[] args) {

		LOG.info("Ist Native   : " + Curve25519.getInstance(Curve25519.BEST).isNative());

		// BEST -> This is a provider that attempts to use NATIVE, but falls back to
		// JAVA if the former is unavailable
		// A Curve25519 interface for generating keys, calculating agreements, creating
		// signatures, and verifying signatures
		Curve25519KeyPair keyPair = Curve25519.getInstance(Curve25519.BEST).generateKeyPair();

		byte[] publicKey = keyPair.getPublicKey();
		byte[] privateKey = keyPair.getPrivateKey();
		LOG.info("Public Key   : " + Format.byteArrayToHex(publicKey));
		LOG.info("Private Key  : " + Format.byteArrayToHex(privateKey));

		Curve25519 cipher = Curve25519.getInstance(Curve25519.BEST);
		// Calculates an ECDH agreement - sharedSecret = A 32-byte shared secret
		byte[] sharedSecret = cipher.calculateAgreement(publicKey, privateKey);
		LOG.info("Shared Secret: " + Format.byteArrayToHex(sharedSecret));

		byte[] message = "Test Nachricht von kleinhirn.eu".getBytes();
		// signature = A 64-byte signature - Calculates a Curve25519 signature
		byte[] signature = cipher.calculateSignature(privateKey, message);
		LOG.info("Signatur     : " + Format.byteArrayToHex(signature));

		// Verify a Curve25519 signature - true if valid, false if not
		boolean validSignature = cipher.verifySignature(publicKey, message, signature);
		Print.ergebnis(validSignature);
	}
}
