package de.wenzlaff.curve25519;

/**
 * Format Funktionen.
 * 
 * @author Thomas Wenzlaff
 *
 */
public class Format {

	public static String byteArrayToHex(byte[] a) {
		StringBuilder sb = new StringBuilder(a.length * 2);
		for (byte b : a)
			sb.append(String.format("%02x", b));
		return sb.toString();
	}
}
