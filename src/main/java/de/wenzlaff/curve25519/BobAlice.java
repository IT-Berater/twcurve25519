package de.wenzlaff.curve25519;

import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.whispersystems.curve25519.Curve25519;
import org.whispersystems.curve25519.Curve25519KeyPair;

/**
 * Testprogram. Alice sendet Bob eine Nachricht. Bob validiert die Nachricht mit
 * dem Public Key von Alice und der Signatur.
 * 
 * @author Thomas Wenzlaff
 *
 */
public class BobAlice {

	private static final Logger LOG = LogManager.getLogger(BobAlice.class);

	public static void main(String[] args) {

		// Bob
		Curve25519 cipherBob = Curve25519.getInstance(Curve25519.BEST);
		Curve25519KeyPair keyPairBob = cipherBob.generateKeyPair();
		byte[] publicKeyBob = keyPairBob.getPublicKey();
		byte[] privateKeyBob = keyPairBob.getPrivateKey();

		// Alice
		Curve25519 cipherAlice = Curve25519.getInstance(Curve25519.BEST);
		Curve25519KeyPair keyPairAlice = cipherAlice.generateKeyPair();
		byte[] publicKeyAlice = keyPairAlice.getPublicKey();
		byte[] privateKeyAlice = keyPairAlice.getPrivateKey();

		// Alice erzeugt ihr shared Secret mit dem Public Key von Bob und ihrem Private
		// Key
		byte[] sharedAlice = cipherAlice.calculateAgreement(publicKeyBob, privateKeyAlice);
		// Bob erzeugt sein shared Secret mit dem Public Key von Alice und seinem
		// Privaten Key
		byte[] sharedBob = cipherBob.calculateAgreement(publicKeyAlice, privateKeyBob);

		// beide erzeugten shared Secret müssen gleich sein
		if (Arrays.equals(sharedAlice, sharedBob)) {
			LOG.info("Alles OK, beide Shared Secret sind gleich");
		}

		// Alice sendet Bob eine Nachricht und die Signatur der Nachricht
		byte[] nachrichtAnBobZuSENDEN = "Nachricht an Bob".getBytes();
		byte[] signatureAliceZuSENDEN = cipherAlice.calculateSignature(privateKeyAlice, nachrichtAnBobZuSENDEN);

		// Alice checkt die Signatur vor dem Absenden (optional)
		boolean validSignature = cipherAlice.verifySignature(publicKeyAlice, nachrichtAnBobZuSENDEN, signatureAliceZuSENDEN);
		Print.ergebnis(validSignature);

		// Bob
		// erhält die Nachricht und die Signatur und überprüft ob die Signatur zur
		// Nachricht passt mit dem public Key von Alice

		boolean validSignatureVonBob = cipherBob.verifySignature(publicKeyAlice, nachrichtAnBobZuSENDEN, signatureAliceZuSENDEN);
		Print.ergebnis(validSignatureVonBob);
	}
}
