package de.wenzlaff.curve25519;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 
 * @author Thomas Wenzlaff
 *
 */
public class Print {

	private static final Logger LOG = LogManager.getLogger(Print.class);

	public static void ergebnis(boolean valid) {
		if (valid) {
			LOG.info("OK: Die Signatur passt zur Nachricht");
		} else {
			LOG.info("ERROR: Die Signatur passt nicht zur Nachricht. Achtung, es wurde was verändert!");
		}
	}
}
