package de.wenzlaff.curve25519;

import org.whispersystems.curve25519.Curve25519;

public class GenerateSharedSecret {

	public static void main(String[] args) {
		Curve25519 cipher = Curve25519.getInstance(Curve25519.BEST);
		System.out.println("Übergabe: publicKey privateKey");
		byte[] publicKey = args[0].getBytes();
		byte[] privateKey = args[1].getBytes();
		// Calculates an ECDH agreement - sharedSecret = A 32-byte shared secret
		byte[] sharedSecret = cipher.calculateAgreement(publicKey, privateKey);
		System.out.println("Shared Secret: " + Format.byteArrayToHex(sharedSecret));
	}
}
