package de.wenzlaff.curve25519;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.whispersystems.curve25519.Curve25519;
import org.whispersystems.curve25519.Curve25519KeyPair;

/**
 * 
 * @author Thomas Wenzlaff
 *
 */
public class GenerateKey {

	private static final Logger LOG = LogManager.getLogger(GenerateKey.class);

	public static void main(String[] args) {

		Curve25519KeyPair keyPair = Curve25519.getInstance(Curve25519.BEST)
				.generateKeyPair();

		byte[] publicKey = keyPair.getPublicKey();
		byte[] privateKey = keyPair.getPrivateKey();
		LOG.info("Public Key : {}", Format.byteArrayToHex(publicKey));
		LOG.info("Private Key: {}", Format.byteArrayToHex(privateKey));
	}
}
